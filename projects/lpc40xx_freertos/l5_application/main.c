#include "FreeRTOS.h"
#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "lpc_peripherals.h"
#include "periodic_scheduler.h"
#include "sj2_cli.h"
#include "task.h"
#include <lpc40xx.h>
#include <semphr.h>
#include <stdio.h>

//#define Part_0
//#define Part_1
#define Part_2

#ifdef Part_0

// void gpio__interrupt();

int main() {

  LPC_GPIO0->DIR &= ~(1 << 29); // Input
  LPC_GPIO2->DIR |= (1 << 3);   // LED Output

  NVIC_EnableIRQ(GPIO_IRQn);
  LPC_GPIOINT->IO0IntEnF |= (1 << 29);

  while (1) {
    // if ((LPC_GPIO0->PIN &= ~(1 << 29))) {
    LPC_GPIO2->PIN |= (1 << 3);
    delay__ms(100);
    LPC_GPIO2->PIN &= ~(1 << 3);
    delay__ms(100);
    fprintf(stderr, "Main Task.\n");
    // }
  }

  return 0;
}

void gpio__interrupt() {
  fprintf(stderr, "Main task is interrupted.\n");
  LPC_GPIOINT->IO0IntClr |= (1 << 29);
  LPC_GPIO2->PIN &= ~(1 << 3);
}

#endif

#ifdef Part_1

void configure_your_gpio_interrupt();
void clear_gpio_interrupt();
void sleep_on_sem_task(void *p);
static SemaphoreHandle_t switch_pressed_signal;
void gpio__interrupt();

int main() {

  switch_pressed_signal = xSemaphoreCreateBinary();
  NVIC_EnableIRQ(GPIO_IRQn);

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio__interrupt, "name");

  configure_your_gpio_interrupt();
  xTaskCreate(sleep_on_sem_task, "sem", (512U * 4) / sizeof(void *), NULL, PRIORITY_LOW, NULL);

  vTaskStartScheduler();
}

void configure_your_gpio_interrupt() {

  LPC_GPIO0->DIR &= ~(1 << 29); // Input
  LPC_GPIO2->DIR |= (1 << 3);   // LED Output
  LPC_GPIOINT->IO0IntEnF |= (1 << 29);
}

void clear_gpio_interrupt() { LPC_GPIOINT->IO0IntClr |= (1 << 29); }

void gpio__interrupt(void) {
  fprintf(stderr, "Give Semaphore.\n");

  xSemaphoreGiveFromISR(switch_pressed_signal, NULL);

  clear_gpio_interrupt();
}

void sleep_on_sem_task(void *p) {
  while (true) {

    if (xSemaphoreTake(switch_pressed_signal, portMAX_DELAY)) {
      fprintf(stderr, "Take Semaphore and Blink LED.\n");
      LPC_GPIO2->PIN |= (1 << 3);
      vTaskDelay(500);
      LPC_GPIO2->PIN &= ~(1 << 3);
      vTaskDelay(500);
    }
  }
}
#endif

#ifdef Part_2
#include "gpio_isr.h"
// void gpio__interrupt(void);
void delay__ms(uint32_t ms);
int main(void) {

  gpio0__attach_interrupt(30, GPIO_INTR__RISING_EDGE, pin30_isr);
  gpio0__attach_interrupt(29, GPIO_INTR__FALLING_EDGE, pin29_isr);

  lpc_peripheral__enable_interrupt(LPC_PERIPHERAL__GPIO, gpio__interrupt, "name");
  NVIC_EnableIRQ(GPIO_IRQn);
  while (1) {
    fprintf(stderr, "You are in Main method.\n");
    delay__ms(1000);
  }
  return 0;
}
#endif